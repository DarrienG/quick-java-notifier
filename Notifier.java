import java.util.ArrayList;
import java.util.List;

interface NotifyMe {
   public void subscribe(EventListener listener);
   public void onEvent();
}

class EventListener {
   List<NotifyMe> toNotify = new ArrayList<>();
   public void addListener(NotifyMe toBeNotified) {
      toNotify.add(toBeNotified);
   }

   public void eventTrigger() {
      for (NotifyMe notifier: toNotify) {
         notifier.onEvent();
      }
   }
}

class ImplementsNotifyMe implements NotifyMe {
   private int number;

   public ImplementsNotifyMe(int number) {
      this.number = number;
   }
   public void subscribe(EventListener listener) {
      listener.addListener(this);
   }

   public void onEvent() {
      System.out.println(number);
   }
}


public class Notifier {
   public static void main(String[] args) {
      EventListener listener = new EventListener();
      ImplementsNotifyMe notifyOne = new ImplementsNotifyMe(1);
      ImplementsNotifyMe notifyTwo = new ImplementsNotifyMe(2);
      ImplementsNotifyMe notifyThree = new ImplementsNotifyMe(3);

      notifyOne.subscribe(listener);
      notifyTwo.subscribe(listener);
      notifyThree.subscribe(listener);

      listener.eventTrigger();
   }
}
